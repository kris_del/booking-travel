import { Component, Vue } from "vue-property-decorator";
import FilterAir from "@/components/fitler-air/filter-air.vue";
import Ticket from "@/components/ticket/ticket.vue";
import { axiosApiInstance } from "@/utils/server";
import Sorting from "@/components/sorting-flights/sorting-flights.vue";
import Loader from "@/components/loader/loader.vue";
import { noBlankLines } from "@/common/common";
import { IFlightOffers, IFormData } from "@/common/interface";

@Component({
  components: {
    FilterAir,
    Ticket,
    Sorting,
    Loader
  }
})
export default class Flights extends Vue {
  data: Array<IFlightOffers> = [];
  isLoading: boolean = false;
  carriers: any = {};
  formData: IFormData = {
    originLocationCode: "",
    destinationLocationCode: "",
    departureDate: "",
    returnDate: "",
    travelClass: "ECONOMY",
    adults: 1,
    children: 0,
    infants: 0,
    currencyCode: "RUB"
  };
  noBlankLines: Function = noBlankLines;
  cityNameDefault = {
    originCode: "",
    destinationCode: ""
  };

  beforeMount(): void {
    if (this.$route.query && Object.keys(this.$route.query).length !== 0) {
      this.isLoading = true;
      this.formData = {
        ...this.formData,
        ...this.$route.query
      };

      this.getFlights(this.$route.query);
    }
  }

  getFlights(data) {
    this.isLoading = true;
    this.data = [];
    axiosApiInstance
      .get("v2/shopping/flight-offers", {
        params: data,
        max: 35
      })
      .then(res => {
        if (res.data && res.data.data && res.data.dictionaries) {
          this.data = res.data.data;
          this.data.forEach(ticket => {
            Vue.set(ticket, "travelClassTicket", this.formData.travelClass);
          });

          this.carriers = res.data.dictionaries.carriers;

          this.getCityName(this.formData.originLocationCode).then(
            (res: any) => {
              this.cityNameDefault.originCode = res[0].name;
            }
          );

          this.getCityName(this.formData.destinationLocationCode).then(
            (res: any) => {
              this.cityNameDefault.destinationCode = res[0].name;
            }
          );

          this.isLoading = false;
        } else {
          this.isLoading = false;
        }
      })
      .catch(err => {
        this.isLoading = false;
        // eslint-disable-next-line no-console
        console.log(err);
      });
  }

  getCityName(cityCode) {
    let url = `https://d5d98elemtiid6nmbq9h.apigw.yandexcloud.net/cities/?city_code=${cityCode}`;

    return new Promise(resolve => {
      fetch(url)
        .then(response => response.json())
        .then(data => {
          resolve(data.airlines);
        });
    });
  }

  submitForm() {
    let query = {
      ...this.filteredFormData,
      max: 10
    };

    this.$router.replace({
      path: "/flights",
      query: query
    });

    this.getFlights(query);
  }

  get filteredFormData() {
    return this.noBlankLines(this.formData);
  }

  sorting(obj) {
    let send = Object.assign(
      {},
      this.filteredFormData,
      (this.formData.includedAirlineCodes = obj)
    );

    this.isLoading = true;
    this.getFlights(send);
  }
}

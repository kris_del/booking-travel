import { Component, Vue } from "vue-property-decorator";
import FilterAir from "@/components/fitler-air/filter-air.vue";
import { IFormData } from "@/common/interface";
import { noBlankLines } from "@/common/common";

@Component({
  components: {
    FilterAir
  }
})
export default class Homepage extends Vue {
  formData: IFormData = {
    originLocationCode: "",
    destinationLocationCode: "",
    departureDate: "",
    returnDate: "",
    travelClass: "ECONOMY",
    adults: 1,
    children: 0,
    infants: 0,
    currencyCode: "RUB"
  };

  noBlankLines: Function = noBlankLines;

  submitForm() {
    let send = this.noBlankLines(this.formData);

    this.$router.push({
      path: "/flights",
      query: {
        ...send,
        max: 35
      }
    });
  }

  beforeMount(): void {
    let currencyAmadeus = localStorage.getItem("currency_amadeus");
    if (currencyAmadeus) {
      this.formData.currencyCode = currencyAmadeus;
    }
  }
}

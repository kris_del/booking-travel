import { Vue, Component } from "vue-property-decorator";
import Loader from "@/components/loader/loader.vue";
import { IFlightOffers } from "@/common/interface";
import Ticket from "@/components/ticket/ticket.vue";
import ReservationCard from "@/views/page-details/components/reservation-card/reservation-card.vue";
import ReservationForm from "@/views/page-details/components/reservation-form/reservation-form.vue";
import { ValidationObserver } from "vee-validate";
import { axiosApiInstance } from "@/utils/server";
import ModelPrint from "@/views/page-details/components/model-print/model-print.vue";

@Component({
  components: {
    Ticket,
    ReservationCard,
    ReservationForm,
    ValidationObserver,
    Loader,
    ModelPrint
  }
})
export default class PageDetails extends Vue {
  flightOffers: IFlightOffers | null = null;
  travelers: any = [];
  isLoading: boolean = false;

  beforeMount(): void {
    this.isLoading = true;
    if (this.$route.query && this.$route.query.ticket) {
      axiosApiInstance
        .post(
          "v1/shopping/flight-offers/pricing",
          {
            data: {
              type: "flight-offers-pricing",
              flightOffers: [JSON.parse(this.$route.query.ticket as string)]
            }
          },
          {
            headers: {
              "X-HTTP-Method-Override": "GET"
            }
          }
        )
        .then(res => {
          this.flightOffers = res.data.data.flightOffers[0];
          ((this.flightOffers || {}).travelerPricings || []).forEach(
            (traveler, index) => {
              this.travelers.push({
                id: index + 1,
                dateOfBirth: "",
                name: {
                  firstName: "",
                  lastName: ""
                },
                documents: "",
                gender: "male",
                contacts: {
                  emailAddress: "",
                  phone: ""
                }
              });
            }
          );
          this.isLoading = false;
        });
    }
  }

  isShowedModel: boolean = false;

  handleSubmit() {
    this.isShowedModel = !this.isShowedModel;
  }

  isClosedModel() {
    this.isShowedModel = false;
  }
}

import { Component, Prop, Vue } from "vue-property-decorator";
import { ITravelerPricings } from "@/common/interface";

@Component
export default class ReservationCard extends Vue {
  @Prop() price: any;
  @Prop() travelerPricings: Array<ITravelerPricings>;

  get translateBaseFare() {
    return {
      ADULT: "Взрослый",
      CHILD: "Ребенок",
      SENIOR: "Пожилой",
      YOUNG: "Молодой",
      HELD_INFANT: "Младенец",
      SEATED_INFANT: "Сидящий младенец",
      STUDENT: "Студент"
    };
  }

  get sumTaxes() {
    let pricing = {
      taxes: 0,
      refundableTaxes: 0
    };
    let sum = 0;
    this.travelerPricings.forEach(traveler => {
      pricing.refundableTaxes += Number(traveler.price.refundableTaxes) || 0;
      (traveler.price.taxes || []).forEach(tax => {
        pricing.taxes += Number(tax.amount || 0);
      });
    });

    return pricing;
  }
}

import { Component, Vue, Prop } from "vue-property-decorator";
import { ValidationProvider } from "vee-validate";
import "@/common/validate";

@Component({
  components: {
    ValidationProvider
  }
})
export default class ReservationForm extends Vue {
  @Prop() id: string;
  @Prop() traveler: any;
}

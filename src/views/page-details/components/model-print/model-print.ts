import { Vue, Component, Prop } from "vue-property-decorator";
import Ticket from "@/components/ticket/ticket.vue";
import { IFlightOffers } from "@/common/interface";

@Component({
  components: {
    Ticket
  }
})
export default class ModelPrint extends Vue {
  @Prop() travelers: any;
  @Prop() flightOffers: IFlightOffers;

  get translateGender() {
    return {
      male: "мужской",
      female: "женский"
    };
  }

  isClosedModel() {
    this.$emit("isClosedModel", false);
  }
}

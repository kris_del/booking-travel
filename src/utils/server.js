import axios from "axios";
import store from "../store/index";

export const axiosApiInstance = axios.create({
  baseURL: "https://test.api.amadeus.com/"
});

axiosApiInstance.interceptors.request.use(
  async config => {
    config.headers = {
      Authorization: "Bearer " + localStorage.getItem("access_token"),
      Accept: "application/json"
    };
    return config;
  },
  error => {
    Promise.reject(error);
  }
);

axiosApiInstance.interceptors.response.use(
  response => {
    return response;
  },
  async function(error) {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      await store.dispatch("getToken");
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + localStorage.getItem("access_token");
      return axiosApiInstance(originalRequest);
    }
    return Promise.reject(error);
  }
);

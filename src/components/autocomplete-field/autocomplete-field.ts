import { Vue, Component, Prop, Watch } from "vue-property-decorator";
import { VueAutosuggest } from "vue-autosuggest";
import { ValidationProvider } from "vee-validate";
import "@/common/validate";

interface Param {
  id: string;
  label: string;
}

@Component({
  components: {
    VueAutosuggest,
    ValidationProvider
  }
})
export default class AutocompleteField extends Vue {
  @Prop() param: Param;
  @Prop({ default: "" }) city: string;

  query: string = "";
  selected: string = "";
  cityUrl: string =
    "https://d5d98elemtiid6nmbq9h.apigw.yandexcloud.net/cities/?city_name=";
  suggestions: any = [];

  async onInputChange() {
    const query = this.query;

    let response = await fetch(this.cityUrl + query);
    let result = await response.json();
    if (result.airlines) {
      this.suggestions = [{ data: result.airlines }];
    }
  }

  renderSuggestion(suggestion) {
    return suggestion.item.name;
  }

  onSelected(item) {
    this.selected = item;
    this.$emit("setAutoCompleteCity", {
      id: this.param.id,
      city: item.item.code
    });
  }

  @Watch("city")
  onCityChanged(val) {
    this.query = val;
  }
}

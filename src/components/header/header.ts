import { Vue, Component } from "vue-property-decorator";
import ClickOutside from "vue-click-outside";
import { namespace } from "vuex-class";
import { ICurrency } from "@/store/modules/dictionary/state";

const dictionaryNS = namespace("dictionary");

@Component({
  directives: {
    ClickOutside
  }
})
export default class Header extends Vue {
  @dictionaryNS.State("listCurrency") listCurrency: Array<ICurrency>;
  isMenuOpened: boolean = false;
  currencyActive: string = "RUB";

  hideMenu() {
    this.isMenuOpened = false;
  }

  handleSelect(currency) {
    this.currencyActive = currency;
    localStorage.setItem("currency_amadeus", currency);
  }

  beforeMount(): void {
    let currencyAmadeus = localStorage.getItem("currency_amadeus");
    if (currencyAmadeus) {
      this.currencyActive = currencyAmadeus;
    }
  }
}

import { Vue, Component, Prop } from "vue-property-decorator";

@Component
export default class sortingFlights extends Vue {
  @Prop() carriers: any;

  isOpenedCarriers: boolean = false;
  checkedCarriers: any = [];

  sortingFlights() {
    let includedAirlineCodes = this.checkedCarriers.join(",");
    this.$emit("sorting", {
      includedAirlineCodes: includedAirlineCodes
    });
  }
}

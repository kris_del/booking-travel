import { Component, Prop, Vue } from "vue-property-decorator";
import AutocompleteField from "@/components/autocomplete-field/autocomplete-field.vue";
import ClickOutside from "vue-click-outside";
import DropdownPassengers from "@/components/fitler-air/dropdown-passengers/dropdown-passengers.vue";
import DropdownClass from "@/components/fitler-air/dropdown-class/dropdown-class.vue";
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";
import "vue2-datepicker/locale/ru";
import { IFormData } from "@/common/interface";
import { ValidationProvider, ValidationObserver } from "vee-validate";
import "@/common/validate";

@Component({
  components: {
    AutocompleteField,
    DropdownPassengers,
    DropdownClass,
    DatePicker,
    ValidationProvider,
    ValidationObserver
  },
  directives: {
    ClickOutside
  }
})
export default class FilterAir extends Vue {
  isOpenedPassengers: boolean = false;
  isOpenedClass: boolean = false;
  @Prop({
    default: () => {
      return {
        originCode: "",
        destinationCode: ""
      };
    }
  })
  cityNameDefault: any;
  @Prop() formData: IFormData;

  setAutoCompleteCity({ id, city }) {
    this.formData[id] = city;
  }

  sumPassengers(payload: { type: string; count: number }) {
    this.formData[payload.type] = payload.count;
  }

  hideDropdownPassengers() {
    this.isOpenedPassengers = false;
  }

  hideDropdownClass() {
    this.isOpenedClass = false;
  }

  notBeforeToday(date: any) {
    return date < new Date(new Date().setHours(0, 0, 0, 0));
  }

  onSubmit() {
    this.$emit("submitForm");
  }
}

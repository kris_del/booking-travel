import { Vue, Component, Prop } from "vue-property-decorator";
import { namespace } from "vuex-class";

const dictionaryNS = namespace("dictionary");

@Component
export default class DropdownClass extends Vue {
  @Prop() formData: any;
  @dictionaryNS.State("travelClass") travelClass;
}

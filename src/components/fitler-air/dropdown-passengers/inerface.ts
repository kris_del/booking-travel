export interface IPassengers {
  adults: number;
  children: number;
  infants: number;
}

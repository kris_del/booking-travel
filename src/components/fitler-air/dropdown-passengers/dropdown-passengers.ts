import { Component, Vue, Prop } from "vue-property-decorator";
import { IPassengers } from "@/components/fitler-air/dropdown-passengers/inerface";

@Component
export default class DropdownPassengers extends Vue {
  @Prop() passengers: IPassengers;

  counterMinus(type: string) {
    this.$emit("sumPassengers", {
      type,
      count: this.passengers[type] = this.passengers[type] - 1
    });
  }

  counterPlus(type: string) {
    this.$emit("sumPassengers", {
      type,
      count: this.passengers[type] = this.passengers[type] + 1
    });
  }

  data = [
    {
      title: "Взрослые",
      sub: "Старше 12 лет",
      type: "adults",
      aria_controls: "search-controls-adults"
    },
    {
      title: "Дети",
      sub: "От 2 до 12 лет",
      type: "children",
      aria_controls: "search-controls-children"
    },
    {
      title: "Младенцы",
      sub: " До 2 лет, без места",
      type: "infants",
      aria_controls: "search-controls-babies"
    }
  ];
}

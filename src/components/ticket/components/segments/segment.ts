import { Vue, Component, Prop } from "vue-property-decorator";
import { namespace } from "vuex-class";
import { IItineraries } from "@/common/interface";

const dictionaryNS = namespace("dictionary");

export interface IFormatDate {
  time: string;
  date: string;
}
@Component
export default class Segment extends Vue {
  @Prop() itinerarie: IItineraries;
  @dictionaryNS.State("cityList") cityList;

  get dataDeparture() {
    let newDate = new Date(this.itinerarie.segments[0].departure.at);
    return this.formatDate(newDate);
  }

  get dataArrival() {
    let newDate = new Date(
      this.itinerarie.segments[this.itinerarie.segments.length - 1].arrival.at
    );

    return this.formatDate(newDate);
  }

  formatDate(date) {
    let inf: IFormatDate = {
      time: "",
      date: ""
    };

    inf.time = date.toLocaleTimeString().slice(0, -3);
    inf.date = date.toLocaleDateString();

    return inf;
  }

  get duration() {
    return this.itinerarie.duration
      ? `${this.itinerarie.duration
          .split("H")[0]
          .slice(2)} ч ${this.itinerarie.duration.split("H")[1].slice(0, -1)} м`
      : "";
  }

  getCurrencyCity(code) {
    if (this.cityList[code]) {
      return this.cityList[code].city_name;
    }
  }
}

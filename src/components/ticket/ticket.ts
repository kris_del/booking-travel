import { Vue, Component, Prop } from "vue-property-decorator";
import Segment from "@/components/ticket/components/segments/segment.vue";
import { TAdditionalServices } from "@/common/interface";
import { namespace } from "vuex-class";
const dictionaryNS = namespace("dictionary");

@Component({
  components: {
    Segment
  }
})
export default class Ticket extends Vue {
  @Prop() ticket: any;
  @dictionaryNS.State("travelClass") travelClassList;
  @dictionaryNS.Action("importCity") importCity;
  sumAdditionalServices: number = 0;
  activeAdditionalServices: Array<TAdditionalServices> = [];

  beforeMount(): void {
    this.importCity();
  }

  addAdditionalServices(type: TAdditionalServices, amount: string) {
    if (this.checkedAdditionalServices(type)) {
      const index = this.activeAdditionalServices.indexOf(type);
      if (index > -1) {
        this.activeAdditionalServices.splice(index, 1);
        this.sumAdditionalServices -= Number(amount);
      }
    } else {
      this.activeAdditionalServices.push(type);
      this.sumAdditionalServices += Number(amount);
    }
  }

  checkedAdditionalServices(type: string) {
    return this.activeAdditionalServices.find((item: string) => {
      return item === type;
    });
  }

  // get travelClassTicket() {
  //   {{this.ticket.travelClassTicket}}
  //   return this.travelClassList.find(
  //     item => item.type === this.ticket.travelClassTicket
  //   ).label;
  // }

  handleSubmit() {
    let routerData = this.$router.resolve({
      path: "/details",

      query: { ticket: JSON.stringify(this.ticket) }
    });

    window.open(routerData.href, "_blank");
  }
}

import { TypeTravelClass } from "@/store/modules/dictionary/state";

export interface IFlightOffers {
  type: string;
  id: string;
  source: string;
  instantTicketingRequired: boolean;
  disablePricing?: string;
  nonHomogeneous: boolean;
  oneWay?: boolean;
  paymentCardRequired?: boolean;
  lastTicketingDate: string;
  numberOfBookableSeats?: number;
  travelClassTicket?: TypeTravelClass;
  itineraries: Array<IItineraries>;
  price: {
    margin?: string;
    grandTotal: string;
    billingCurrency?: string;
    additionalServices?: Array<IAdditionalServices>;
    currency: string;
    total: string;
    base: string;
    fees?: Array<{ amount: string; type: string }>;
    taxes?: Array<{ amount: string; code: string }>;
    refundableTaxes?: string;
  };
  pricingOptions: {
    includedCheckedBagsOnly: boolean;
    refundableFare?: boolean;
    noRestrictionFare?: boolean;
    noPenaltyFare?: boolean;
    fareType: Array<string>;
  };
  validatingAirlineCodes: Array<string>;
  travelerPricings: Array<ITravelerPricings>;
}

export interface IItineraries {
  duration?: string;
  segments: Array<ISegments>;
}

export interface ISegments {
  id: string;
  numberOfStops: number;
  blacklistedInEU?: boolean;
  departure: {
    iataCode: string;
    terminal?: string;
    at: string;
  };
  arrival: {
    iataCode: string;
    terminal?: string;
    at: string;
  };
  carrierCode: string;
  number: string;
  aircraft: {
    code: string;
  };
  operating: {
    carrierCode: string;
  };
  duration: string;
  co2Emissions?: Array<{ weight: number; weightUnit: string; cabin: string }>;
}

export interface IAdditionalServices {
  amount: string;
  type: TAdditionalServices;
}

export type TAdditionalServices =
  | "CHECKED_BAGS"
  | "MEALS"
  | "SEATS"
  | "OTHER_SERVICES";

export interface ITravelerPricings {
  travelerId: string;
  fareOption: string;
  travelerType: string;
  associatedAdultId?: string;
  price: {
    currency: string;
    total: string;
    base: string;
    fees?: Array<{ amount: string; type: string }>;
    taxes?: Array<{ amount: string; code: string }>;
    refundableTaxes?: string;
  };
  fareDetailsBySegment: Array<{
    segmentId: string;
    cabin: string;
    fareBasis: string;
    brandedFare?: string;
    class: string;
    isAllotment?: boolean;
    includedCheckedBags: {
      weight?: number;
      weightUnit?: string;
      quantity?: number;
    };
    additionalServices?: {
      chargeableCheckedBags?: {
        weight?: number;
        weightUnit?: string;
        quantity?: number;
      };
      chargeableSeatNumber?: string;
      otherServices?: Array<string>;
    };
  }>;
}

export interface IFormData {
  originLocationCode: string;
  destinationLocationCode: string;
  departureDate: string | null;
  returnDate?: string | null;
  adults: number;
  children?: number;
  infants?: number;
  travelClass: TypeTravelClass;
  currencyCode: string;
  includedAirlineCodes?: string;
}

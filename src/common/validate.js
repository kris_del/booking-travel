import { email, alpha, numeric } from "vee-validate/dist/rules";
import { extend } from "vee-validate";

extend("min", {
  validate(value, args) {
    return value.length >= args.length;
  },
  params: ["length"],
  message: "Поле должно содержать более {length} символов"
});

extend("alpha", {
  ...alpha,
  message: "Используйте только латинские буквы"
});

extend("email", {
  ...email,
  message: "Введите валидный email"
});

extend("numeric", {
  ...numeric,
  message: "Поля может содержать только цифры"
});

extend("required", {
  validate: value => {
    return {
      required: true,
      valid: ["", null, undefined].indexOf(value) === -1
    };
  },
  // This rule reports the `required` state of the field.
  computesRequired: true
});

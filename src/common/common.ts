export function noBlankLines(obj: any) {
  let send: any = {};

  for (let key in obj) {
    if (obj[key] !== null && obj[key] !== "") {
      send[key] = obj[key];
    }
  }

  return send;
}

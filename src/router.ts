import Vue from "vue";
import VueRouter from "vue-router";
import store from "./store";

Vue.use(VueRouter);
import PageLayout from "@/layouts/page-layout.vue";

const Homepage = () =>
  import(
    /* webpackChunkName: "homepage-view" */ "@/views/homepage/homepage.vue"
  );
const FlightsPage = () =>
  import(/* webpackChunkName: "flights-view" */ "@/views/flights/flights.vue");
const PageDetails = () =>
  import(
    /* webpackChunkName: "details-view" */ "@/views/page-details/page-details.vue"
  );
const Page404 = () =>
  import(
    /* webpackChunkName: "page-404-view" */ "@/views/page-404/page-404.vue"
  );

const router = new VueRouter({
  mode: "history",
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: "/",
      component: PageLayout,
      beforeEnter(to, from, next) {
        if (localStorage.getItem("token_amadeus")) {
          next();
        } else if (!localStorage.getItem("token_amadeus")) {
          store.dispatch("getToken").then(() => {
            next();
          });
        }
      },
      children: [
        {
          path: "",
          name: "index",
          component: Homepage
        },
        {
          path: "/flights",
          name: "flights",
          component: FlightsPage
        },
        {
          path: "/details",
          name: "details",
          component: PageDetails
        },
        {
          path: "*",
          name: "404",
          component: Page404
        }
      ]
    }
  ]
});

export default router;

import Vue from "vue";
import Vuex from "vuex";
import dictionary from "./modules/dictionary/index";
import ticket from "./modules/ticket/index";

Vue.use(Vuex);

const store = new Vuex.Store<any>({
  modules: {
    dictionary,
    ticket
  },
  getters: {},
  actions: {
    getToken() {
      let data = {
        client_id: "x92xqn2k9527onr1uGRQOnahnnSFbMuH",
        client_secret: "KnHdXDD37mLwqueE",
        grant_type: "client_credentials"
      };

      let urlencoded = new URLSearchParams();
      urlencoded.append("client_id", data.client_id);
      urlencoded.append("client_secret", data.client_secret);
      urlencoded.append("grant_type", data.grant_type);

      let requestOptions = {
        method: "POST",
        headers: {
          "Content-type": "application/x-www-form-urlencoded"
        },
        body: urlencoded
      };

      fetch(
        "https://test.api.amadeus.com/v1/security/oauth2/token",
        requestOptions
      )
        .then(response => response.json())
        .then(data => {
          localStorage.setItem("access_token", data.access_token);
        })
        // eslint-disable-next-line no-console
        .catch(error => console.log("error", error));
    }
  },
  state: {},
  mutations: {}
});

export default store;

import state from "@/store/modules/ticket/state";
export default {
  namespaced: true,
  state,
  mutations: {
    setTicket: (state, ticket) => {
      state.selectedTicket = ticket;
    }
  },
  actions: {},
  getters: {}
};

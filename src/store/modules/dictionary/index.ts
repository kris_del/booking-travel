import state from "./state";
import cityListJson from "./city.json";

export default {
  namespaced: true,
  state,
  mutations: {
    setCityListJson: (state, result) => {
      state.cityList = result;
    }
  },
  actions: {
    importCity: context => {
      let cities = cityListJson;
      let result = {};
      for (let i = 0; i < cities.length; i++) {
        let iata_code = cities[i].code;
        result[iata_code] = { city_name: cities[i].city_name };
      }
      context.commit("setCityListJson", result);
    }
  },
  getters: {}
};

export interface ITravelClass {
  label: string;
  type: TypeTravelClass;
}

export type TypeTravelClass =
  | "ECONOMY"
  | "PREMIUM_ECONOMY"
  | "BUSINESS"
  | "FIRST";

export interface ICurrency {
  code: string;
  label: string;
}

export interface IDictionaryState {
  travelClass: Array<ITravelClass>;
  cityList: any;
  listCurrency: Array<ICurrency>;
}

export default (): IDictionaryState => ({
  cityList: {},
  travelClass: [
    {
      label: "Эконом",
      type: "ECONOMY"
    },
    {
      label: "Комфорт",
      type: "PREMIUM_ECONOMY"
    },
    {
      label: "Бизнес",
      type: "BUSINESS"
    },
    {
      label: "Первый класс",
      type: "FIRST"
    }
  ],
  listCurrency: [
    {
      code: "RUB",
      label: "Рубли"
    },
    {
      code: "EUR",
      label: "Евро"
    },
    {
      code: "USD",
      label: "Доллар"
    }
  ]
});

Шаблон веб-приложения на VueJS

## Архитектура
Проект построен на [TypeScript](https://www.typescriptlang.org/) и реактивном фреймворке [Vue](https://vuejs.org/). Приложение во время выполнения хранит данные в памяти во [Vuex](https://vuex.vuejs.org/), также используются библиотеки [vuex-module-decorators](https://github.com/championswimmer/vuex-module-decorators) и [vue-property-decorator](https://github.com/kaorun343/vue-property-decorator) для удобства интеграции Vue с TypeScript'ом и повышения качества типизации кода.

Основная архитектурная идея состоит в 4 сущностях:
* __Компоненты__, находятся в папке `src/components`, обязаны отрисовывать графические элементы интерфейса пользователя, должны содержать насколько возможно минимум самостоятельной логики и общаться с окружением только через пропы и события. _Не должны взаимодействовать со стором напрямую._ _Не должны взаимодействовать с API клиентами напрямую._
* __Страницы__, находятся в папке `src/views`, обычно подключаются в роутер, кем и отрисовываются, обязаны отрисовывать страницу приложения, могут использовать в себе компоненты, могут общаться со стором напрямую.
* __Vuex Store__, находятся в папке `src/store`, конкретные модули именуются с постфиксом Module в именах классов и файлов.

## Структура папок
* `public` -- статические некомпилируемые файлы проекта
* `src/assets` -- компилируемые файлы ассетов
* `src/components` -- переиспользуемые компоненты
* `src/router.ts` -- конфигурация роутинга
* `src/store` -- конфигурация и подключение Vuex стора
* `src/views` -- компоненты страниц, подключаются на роуты в router
* `src/app.vue` -- корневой компонент проекта
* `src/main.ts` -- точка входа проекта

## Требования к среде разработки
* [NodeJS](https://nodejs.org/) - рекомендуется версия 12 и выше
* [NPM](https://www.npmjs.com/) - обычно поставляется вместе с NodeJS, рекомендуется версия 6 и выше


## Зависимости

## Начало работы
### Шаг 1
Для установки зависимостей переходим в корневую директорию проекта и запускаем команду:
```sh
npm install
```

### Шаг 2
Теперь можно запустить локальный сервер разработки выполнив команду:
```sh
npm run serve
```
